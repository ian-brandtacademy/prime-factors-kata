package academy.brandt.kata

fun primeFactorsOf(n: Int): List<Int> {

	var num = n
	var divisor = 2
	val primeFactors = mutableListOf<Int>()

	while (num > 1) {
		while (num % divisor == 0) {
			primeFactors.add(divisor)
			num /= divisor
		}
		divisor++
	}

	if (num > 1) primeFactors.add(num)

	return primeFactors
}
