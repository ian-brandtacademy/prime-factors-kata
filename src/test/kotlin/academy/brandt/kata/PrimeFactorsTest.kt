package academy.brandt.kata

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class PrimeFactorsTest {

	@Test
	fun `test prime factorization of 1`() {
		assertThat(primeFactorsOf(1)).isEqualTo(emptyList<Nothing>())
	}

	@Test
	fun `test prime factorization of 2`() {
		assertThat(primeFactorsOf(2)).isEqualTo(listOf(2))
	}

	@Test
	fun `test prime factorization of 3`() {
		assertThat(primeFactorsOf(3)).isEqualTo(listOf(3))
	}

	@Test
	fun `test prime factorization of 4`() {
		assertThat(primeFactorsOf(4)).isEqualTo(listOf(2, 2))
	}

	@Test
	fun `test prime factorization of 5`() {
		assertThat(primeFactorsOf(5)).isEqualTo(listOf(5))
	}

	@Test
	fun `test prime factorization of 6`() {
		assertThat(primeFactorsOf(6)).isEqualTo(listOf(2, 3))
	}

	@Test
	fun `test prime factorization of 7`() {
		assertThat(primeFactorsOf(7)).isEqualTo(listOf(7))
	}

	@Test
	fun `test prime factorization of 9`() {
		assertThat(primeFactorsOf(9)).isEqualTo(listOf(3, 3))
	}

	@Test
	fun `test prime factorization of 8`() {
		assertThat(primeFactorsOf(8)).isEqualTo(listOf(2, 2, 2))
	}

	@Test
	fun `test prime factorization of 10`() {
		assertThat(primeFactorsOf(10)).isEqualTo(listOf(2, 5))
	}

	@Test
	fun `test prime factorization of 12`() {
		assertThat(primeFactorsOf(12)).isEqualTo(listOf(2, 2, 3))
	}

	@Test
	fun `test prime factorization of a big number`() {
		val bigNumber = 2 * 2 * 3 * 3 * 5 * 7 * 11 * 13 * 13 * 17
		println(bigNumber)
		assertThat(primeFactorsOf(bigNumber)).isEqualTo(listOf(2, 2, 3, 3, 5, 7, 11, 13, 13, 17))
	}
}
